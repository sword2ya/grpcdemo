// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"context"
	"fmt"
	"grpcdemo/demo_proto"
	"net"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.MinimumNArgs(1),

	Run: func(cmd *cobra.Command, args []string) {
		startServer(args)
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serverCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serverCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func startServer(args []string) {
	conn, err := net.Listen("tcp", args[0])
	if err != nil {
		fmt.Println("listen failed!", err)
		return
	}
	server := grpc.NewServer()
	demo_proto.RegisterDemoServer(server, &demoServer{})
	reflection.Register(server)

	fmt.Println("server is ready to start")
	if err := server.Serve(conn); err != nil {
		fmt.Println("serve failed!", err)
	}
}

type demoServer struct {
}

func (server *demoServer) Register(ctx context.Context, request *demo_proto.RegisterRequest) (*demo_proto.RegisterResponse, error) {
	msg := fmt.Sprintf("Hello, %s. registed success.", request.GetName())
	fmt.Printf("new register, name:%s, msg:%s\n", request.GetName(), msg)
	return &demo_proto.RegisterResponse{Message: msg}, nil
}
