// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"context"
	"fmt"
	"grpcdemo/demo_proto"

	consulapi "github.com/hashicorp/consul/api"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
)

const (
	defaultName = "abcd"
)

// registerCmd represents the register command
var registerCmd = &cobra.Command{
	Use:   "register",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		register(args)
	},
}

func init() {
	rootCmd.AddCommand(registerCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// registerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// registerCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func register(args []string) {
	serviceAddr, err := getServerAddr(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}
	conn, err := grpc.Dial(serviceAddr, grpc.WithInsecure())
	if err != nil {
		fmt.Println("dial failed!", err)
		return
	}
	defer conn.Close()
	client := demo_proto.NewDemoClient(conn)
	name := defaultName
	if len(args) > 1 {
		name = args[1]
	}
	response, err := client.Register(context.Background(), &demo_proto.RegisterRequest{Name: name})
	fmt.Printf("register response, msg: %s \n", response.GetMessage())
}

func getServerAddr(serverName string) (string, error) {
	client, err := consulapi.NewClient(consulapi.DefaultConfig())
	if err != nil {
		return "", err
	}
	catalog := client.Catalog()
	services, _, err := catalog.Service(serverName, "", nil)
	if err != nil {
		return "", err
	}
	if len(services) < 1 {
		return "", fmt.Errorf("no service named %s", serverName)
	}
	service := services[0]
	return fmt.Sprintf("%s:%d", service.Address, service.ServicePort), nil
}
