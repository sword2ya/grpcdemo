# gRPC DEMO
gRPC DEMO简单描述了三个框架的使用，分别是cobra、gRPC和consul

cobra是go语言的命令行框架，详细信息参考： https://github.com/spf13/cobra  
gRPC是google使用go语言开发的RPC框架，详细信息参考： https://grpc.io/  
consul是一个分布式服务发现系统，详细信息参考： https://www.consul.io/

gRPC DEMO使用了第三方为consul提供的go api来发现服务，具体参考： https://godoc.org/github.com/hashicorp/consul/api

# 快速开始

1. 按照官方文档consul,cobra以及gRPC
2. 配置好consul的服务

```bash   
mkdir /etc/consul.d
echo {"service": {"name": "grpcdemo", "tags": ["demo"], "port":6666}} > /etc/consul.d
``` 
3. 启动consul。`consul agent -dev -config-dir=/etc/consul.d`
4. 启动gRPC DEMO服务器。 `go run main.go server 0.0.0.0:6666`
5. 启动gRPC DEMO客户端。 `go run main.go register grpcdemo abcd`
6. 在客户端将会看到输出：“register response, msg: Hello, abcd. registed success.”。同时在服务器将会看到输出： “new register, name:abcd, msg:Hello, abcd. registed success.”

# Demo实现

## 服务的创建和命令添加
1. 使用cobra命令行工具创建了grpcdemo服务

```
cobra init grpcdemo
```
2. 使用cobra命令行工具生成server和register命令的基础代码

```
cobra add server
cobra add register
```

## proto
1. 在$GOPATH/src/grpcdemo/demo_proto目录创建demo.proto文件，创建RPC接口。demo.proto文件内容如下：

```proto 
syntax = "proto3";

package demo_proto;

service Demo {
    rpc Register(RegisterRequest) returns (RegisterResponse) {}
}

message RegisterRequest {
    string name = 1;
}

message RegisterResponse {
    string message = 1;
}
```
简单起见，demo.proto提供一个服务Demo，Demo中提供了一个方法Register。它输入一个name并且返回一个message。  
2. 使用下面命令生成demo.pb.go文件

```bash  
cd $GOPATH/src/grpcdemo/demo_proto
protoc -I ./ ./demo.proto --go_out=plugins=grpc:.
```
## register.go代码

1. getServerAddr函数根据serverName从consul中获取其服务地址。consulapi是consul api包的别名

```go   
func getServerAddr(serverName string) (string, error) {
	client, err := consulapi.NewClient(consulapi.DefaultConfig())
	if err != nil {
		return "", err
	}
	catalog := client.Catalog()
	services, _, err := catalog.Service(serverName, "", nil)
	if err != nil {
		return "", err
	}
	if len(services) < 1 {
		return "", fmt.Errorf("no service named %s", serverName)
	}
	service := services[0]
	return fmt.Sprintf("%s:%d", service.Address, service.ServicePort), nil
}
```

2. register函数首先根据参数获取服务地址，然后创建gRPC客户端，再调用Register RPC函数，最后将Register函数返回的Message打印到标准输出。

```go   
func register(args []string) {
	serviceAddr, err := getServerAddr(args[0])
	if err != nil {
		fmt.Println(err)
		return
	}
	conn, err := grpc.Dial(serviceAddr, grpc.WithInsecure())
	if err != nil {
		fmt.Println("dial failed!", err)
		return
	}
	defer conn.Close()
	client := demo_proto.NewDemoClient(conn)
	name := defaultName
	if len(args) > 1 {
		name = args[1]
	}
	response, err := client.Register(context.Background(), &demo_proto.RegisterRequest{Name: name})
	fmt.Printf("register response, msg: %s \n", response.GetMessage())
}
```

## server.go代码
1. 定义demoServer结构，并且实现DemoServer接口。Register简单的根据输入的名字拼接一个字符串然后返回。

```go   
type demoServer struct {
}

func (server *demoServer) Register(ctx context.Context, request *demo_proto.RegisterRequest) (*demo_proto.RegisterResponse, error) {
	msg := fmt.Sprintf("Hello, %s. registed success.", request.GetName())
	fmt.Printf("new register, name:%s, msg:%s\n", request.GetName(), msg)
	return &demo_proto.RegisterResponse{Message: msg}, nil
}
```

2. startServer用来启动服务。它首先根据参数创建一个Listen连接，然后创建gRPC服务并且注册DemoServer，最后使用创建的服务在Listen连接上处理RPC请求。

```go   
func startServer(args []string) {
	conn, err := net.Listen("tcp", args[0])
	if err != nil {
		fmt.Println("listen failed!", err)
		return
	}
	server := grpc.NewServer()
	demo_proto.RegisterDemoServer(server, &demoServer{})
	reflection.Register(server)

	fmt.Println("server is ready to start")
	if err := server.Serve(conn); err != nil {
		fmt.Println("serve failed!", err)
	}
}
```
